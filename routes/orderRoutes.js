const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/OrderController.js');
const auth = require('../auth.js');

const {verify, verifyAdmin} = auth;

// Non-admin user checkout (Create Order)
router.post('/checkout', verify, (request, response) => {
	OrderController.createOrder(request, response);
})

// Get all orders
router.get('/all', verify, verifyAdmin, (request, response) => {
	OrderController.getAllOrders(request, response);
})

//[SECTION] Additional functionalities
// Get orders by specific user
router.get('/:userId', verify, (request, response) => {
	OrderController.getUserOrders(request, response);
})


module.exports = router;