const express = require('express');
const router = express.Router();
const auth = require('../auth.js');
const ProductController = require('../controllers/ProductController.js');

const {verify, verifyAdmin} = auth;

// Create Product (Admin Only)
router.post('/add', verify, verifyAdmin, (request, response) => {
	ProductController.createProduct(request, response);
})

// Retrieving all products
router.get('/all', (request, response) => {
	ProductController.getAllProducts(request, response);
})

// Retrieving all ACTIVE products
router.get('/', (request, response) => {
	ProductController.getAllActiveProducts(request, response);
})

// Retrieving a single product by its ID
router.get('/:productId', (request, response) => {
	ProductController.getSpecificProduct(request, response);
})

// Updating product information (Admin only)
router.put('/:productId', verify, verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
})

// Archiving a product (Admin only)
router.put('/:productId/archive', (request, response) => {
	ProductController.archiveProduct(request, response);
})

// Activating a product
router.put('/:productId/activate', (request, response) => {
	ProductController.activateProduct(request, response);
})



// [SECTION] Additional functionalities for checking data


module.exports = router;