const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js');
const auth = require('../auth.js');
const {verify, verifyAdmin, verifyUserAndAdmin} = auth;

// User Registration
router.post('/register', (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result);
	})
})


// User Authentication
router.post('/login', (request, response) => {
	UserController.loginUser(request, response);
})

// Check if an email already exists
router.post('/check-email', (request, response) => {
	UserController.checkEmailExists(request.body).then((result) => {
		response.send(result);
	})
})

// [CART]
router.post('/:id', verifyUserAndAdmin, (request, response) => {
	UserController.addToCart(request, response);
})



// GET all users (Additional functionality for checking data)
router.get('/all', verify, verifyAdmin, (request, response) => {
	UserController.getAllUsers(request, response);
	// .then((result) => {response.send(result);})
})

// Retrieve user details
router.get('/details', verify, (request, response) => {
	UserController.getUser(request, response);
})

// Retrieve user's orders
router.get('/:id/my-orders', verifyUserAndAdmin, (request, response) => {
	UserController.getOrders(request, response);
})

// Set user as admin (Admin only)
router.put('/:id/set-admin', verify, verifyAdmin, (request, response) => {
	UserController.setAdmin(request, response);
})

// [SECTION] OPTIONAL - Additional functionality for checking data
// Set user as non-admin (Admin only)
router.put('/:id/set-non-admin', verify, verifyAdmin, (request, response) => {
	UserController.setToNonAdmin(request, response);
})





module.exports = router;