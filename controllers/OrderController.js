const Order = require('../models/Order.js');
const Product = require('../models/Product.js');


// Create Order
module.exports.createOrder = async (request, response) => {
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	try {
        const product = await Product.findById(request.body.productId);
        if (!product || product.isActive == false) {
            return response.send({
                message: "Product does not exist! Product may have been sold out or deleted."
            });
        }

        let new_order = new Order({
            userId: request.user.id,
            email: request.user.email,
            products: {
                productId: request.body.productId,
                productName: request.body.productName,
                quantity: request.body.quantity
            }
        });

        console.log(request.body.productName);

        const totalAmount = await new_order.calculateTotalAmount();
        return new_order.save().then((saved_order, error) => {
			/*if(error){
				return response.send({
					message: error.message
				});
			}

			return response.send({
				message: "Order created successfully!",
				data: saved_order
			});*/
			if(error){
				return response.send(false);
			}

			return response.send(true);
		});
    } catch (error) {
        return response.send({
            message: "Product does not exist!"
        });
    }
}


// Get all orders
module.exports.getAllOrders = (request, response) => {
	return Order.find({}).then((result) => {
		return response.send(result);
	});
}


// [SECTION] Additional functionalities
// Get orders by specific user
const User = require('../models/User.js');
module.exports.getUserOrders = (request, response) => {
	return Order.find({ userId: request.params.userId }).then(result => {
		console.log(result);
		return response.send(result);
	})
}
