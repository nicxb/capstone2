const Product = require('../models/Product.js');

module.exports.createProduct = (request, response) => {
	return Product.findOne({name: request.body.name}).then(result => {
		if(result){
			return response.send({
				message: "Product already exists!"
			});
		}

		let new_product = new Product({
			name: request.body.name,
			description: request.body.description,
			price: request.body.price
		});

		return new_product.save().then((saved_product, error) => {
			if(error){
				return response.send({
					message: "A problem has occurred in creating the product. Please try again."
				});
			}

			return response.send({
				message: "Product is succesfully created and is now active."
			});
		}).catch(error => response.send(error));
	})
}

// Retrieving all products
module.exports.getAllProducts = (request, response) => {
	return Product.find({}).then(result => {
		return response.send(result);
	});
}

// Retrieve all Active Products
module.exports.getAllActiveProducts = (request, response) => {
	return Product.find({ isActive: true }).then(result => {
		return response.send(result);
	})
	.catch(error => response.send(error));
}

// Retrieving a single product by its ID
module.exports.getSpecificProduct = (request, response) => {
	// return Product.findById(request.params.productId).then(result => {
	// 	return response.send(result);
	// });
	return Product.findById(request.params.productId).then(result => {
		
		if(result.isActive === false) {
			return response.send(false)
		}
		return response.send(result);
	});
}

// Updating a product (Admin only)
module.exports.updateProduct = (request, response) => {
	let updated_product = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

	return Product.findByIdAndUpdate(request.params.productId, updated_product).then((product, error) => {
		/*if(error){
			return response.send({
				message: error.message
			});
		}

		return response.send({
			message: "Product has been updated successfully!"
		});*/

		if(error){
			return response.send(false);
		}

		return response.send(true);
	})
}

// Archiving a product (Admin only)
module.exports.archiveProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.productId, {isActive: false})
	.then((product, error) => {
		/*if(error){
			return response.send({
				false
			});
		}

		return response.send({
			message: "Product archived successfully"
		});*/

		if(error) {
			return response.send(false);
		}

		return response.send(true);
	})
}

// Activate a product (Admin only)
module.exports.activateProduct = (request, response) => {
	return Product.findByIdAndUpdate(request.params.productId, {isActive: true})
	.then((product, error) => {
		/*if(error){
			return response.send({
				message: error.message
			});
		}

		return response.send({
			message: "Product activated successfully"
		});*/
		if(error){
			return response.send(false);
		}

		return response.send(true);
	})
}

// [SECTION] Additional functionalities for checking of data

