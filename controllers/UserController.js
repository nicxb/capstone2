const User = require('../models/User.js');
const Order = require('../models/Order.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');



// User Registration
module.exports.registerUser = (request_body) => {
	return User.findOne({ email: request_body.email }).then(result => {
		if(result) {
			return {
				message: "The user already exists!"
			};
		}

		let new_user = new User({
			firstName: request_body.firstName,
			lastName: request_body.lastName,
			email: request_body.email,
			mobileNo: request_body.mobileNo,
			// password: request_body.password
			password: bcrypt.hashSync(request_body.password, 10) 
		});

		return new_user.save().then((registered_user, error) => {
			/*if(error){
				return {
					message: error.message
				};
			}*/

			/*return {
				message: "Successful User Authentication! User has been registered.",
			};*/
			if(error){
				return false;
			}

			return true;

		}).catch(error => console.log(error));
	})
}

// Check if email already exists
module.exports.checkEmailExists = (request_body) => {
	return User.find({ email: request_body.email }).then((result, error) => {
		if(result.length > 0) {
			return true;
		}

		return false;
	})
}

// User Authentication
module.exports.loginUser = (request, response) => {
	return User.findOne({ email: request.body.email }).then(result => {
		/*if(result == null) {
			return response.send({
				message: "The user is not registered yet. Check your email and password again."
			});
		}*/
		if( result == null ) {
			return false;
		}

		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		/*if(isPasswordCorrect){
			console.log(result.email);

			return response.send({ 
				accessToken: auth.createAccessToken(result) 
			});
		} else {
			return response.send({
				message: "Your password is incorrect."
			});
		}*/

		if( isPasswordCorrect ) {
			console.log(result.email);
			return response.send({ 
				accessToken: auth.createAccessToken(result) 
			}); 
		} else {
			return false;
		}

	}).catch(error => response.send(error));
}

// Retrieve User details
/*module.exports.getUser = (request, response) => {
	return User.findById(request.params.id).then((result => {
			result.password = '';
			return response.send(result);
		})).catch(error => console.log(error));
}*/

// Route for retrieving User's authentication
module.exports.getUser = (request, response) => {
	return User.findById(request.user.id).then((result => {
			result.password = "";
			return response.send(result);
		})).catch(error => console.log(error));
}

// Set user as admin (Admin only)
module.exports.setAdmin = (request, response) => {
	return User.findByIdAndUpdate(request.params.id, {isAdmin: true}).then((user, error) => {
		/*if(error){
			return response.send({
				message: error.message
			});
		}

		return response.send({
			message: "User has been set as administrator successfully!"
		});*/

		if(error){
			return response.send(false);
		}

		return response.send(true);
	});
}

// Retrieve user's orders

module.exports.getOrders = (request, response) => {
	Order.find({userId: request.params.id})
	.then((result) => {
		return response.send(result);
	})
}


// [SECTION] Additional functionalities for checking data
// Get all users
module.exports.getAllUsers = (request, response) => {
	return User.find({}).then((result) => {
		return response.send(result);
	}).catch(error => console.log(error));
}

// Set user as non-admin (Admin only)
module.exports.setToNonAdmin = (request, response) => {
	return User.findByIdAndUpdate(request.params.id, {isAdmin: false}).then((user, error) => {
		/*if(error){
			return response.send({
				message: error.message
			});
		}

		return response.send({
			message: "User has been set as non-administrator successfully!"
		});*/
		if(error){
			return response.send(false);
		}

		return response.send(true);
	});
}


//[CART]
// add to cart
module.exports.addToCart = async (request, response) => {
	if(request.user.isAdmin){
		return response.send('Action Forbidden');
	}

	// Updating user's cart
	return User.findById(request.user.id).then(user => {
		let new_item = {
			productId: request.body.productId,
			quantity: request.body.quantity
		}


		user.cart.push(new_item);

		return user.save().then((updated_user, error) => {
			if(error){
				return response.send({
					message: error.message
				});
			}

			return response.send({
				message: "Product added to cart successfully!"
			});
		});
	})
}