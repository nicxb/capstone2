const mongoose = require('mongoose');
const Product = require('./Product.js');


const order_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: [ true, "UserId is required"]
	},
	email: {
		type: String,
		required: [ true, "First Name is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [ true, "ProductId is required"]
			},
			productName: {
				type: String,
				required: [ true, "Product name is required"]
			},
			quantity: {
				type: Number
			}
		}
	],
	totalAmount: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

order_schema.methods.calculateTotalAmount = async function () {
  const productPrices = await Promise.all(
    this.products.map(async product => {
      const foundProduct = await Product.findById(product.productId);
      return foundProduct.price * product.quantity;
    })
  );

  const totalAmount = productPrices.reduce((sum, price) => sum + price, 0);
  this.totalAmount = totalAmount;
  return totalAmount;
};





module.exports = mongoose.model("Order", order_schema);