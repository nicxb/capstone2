const jwt = require('jsonwebtoken');
const secret_key = "ECommerceBasnilloB303";

// Generating a token
module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(user_data, secret_key, {});
}


// Verifying a token
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;

	if(typeof token === "undefined"){
		return response.send({
			auth: 'Failed, please include token in the header of the request.'
		})
	}

	token = token.slice(7, token.length);
	jwt.verify(token, secret_key, (error, decoded_token) => {
		if(error){
			return response.send({
				auth: 'Failed',
				message: error.message
			})
		}

		console.log(decoded_token);
		request.user = decoded_token;

		next();
	})
}


// Verifying if user is admin
module.exports.verifyAdmin = (request, response, next) => {
	if(request.user.isAdmin){
		return next();
	}

	return response.send({
		auth: 'Failed',
		message: 'Action Forbidden'
	});
}

// Verifying authenticated user-only and admin-only
module.exports.verifyUserAndAdmin = (request, response, next) => {
	let token = request.headers.authorization;

	if(typeof token === "undefined"){
		return response.send({
			auth: 'Failed, please include token in the header of the request.'
		})
	}

	token = token.slice(7, token.length);
	jwt.verify(token, secret_key, (error, decoded_token) => {
		if(error){
			return response.status(403).json({
				auth: 'Failed',
				message: error.message
			})
		}

		console.log(decoded_token);
		request.user = decoded_token;

		if(request.user.isAdmin){
			return next();
		}

		if(decoded_token.id !== request.params.id) {
			return response.status(403).json({
				auth: 'Failed',
				message: 'Token ID does not match the user ID. You do not have the permission to access orders.'
			})
		}

		next();
	})
}