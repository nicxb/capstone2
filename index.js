// Server Variables
const express = require('express');
const app = express();
const port = 4000;
const mongoose = require('mongoose');
require('dotenv').config();
const cors = require('cors');

const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');



// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


// Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);
// app.use('/users', userRoutes);
// app.use('/products', productRoutes);
// app.use('/orders', orderRoutes);




// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:admin1234@303-basnillo.kk8ki9l.mongodb.net/capstone2?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology:true
});
let database = mongoose.connection;
database.on('error', () => console.log("Can't connect to database."));
database.once('open', () => console.log('Connected to MongoDB!'));


// Server listening
app.listen(process.env.PORT || port, () => {console.log(`Capstone2-Basnillo is now running at localhost:${process.env.PORT || port}`);
});

module.exports = app;